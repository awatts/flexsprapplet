[![License](http://img.shields.io/badge/license-BSD-red.svg)](http://choosealicense.com/licenses/bsd-2-clause/)

# Compiling the applet

## Flex 3 version (don't use this anymore)
To get debugging symbols included, compile with

    # set "flex3bin" to where you have the Flex3 SDK installed
    export flex3bin='/usr/local/flex_sdk_3.6a/bin/'
    ${flex3bin}/mxmlc -compiler.debug flexspr_3.mxml

## Flex 4 version:

    # set "flexlib" to where you have the Flex4 SDK installed
    # assumes that the Flex4 SDK bin directory is already on your path
    export flexlib='/usr/local/flex_sdk_4.16.0/frameworks/'
    mxmlc -theme=${flexlib}/themes/Halo/halo.swc flexspr_4.mxml

# External functions

The Flash applet calls several external JavaScript functions (if defined) for events that occur.

To aid in debugging, at several points the applet will log its state. The most sensible thing to do on the JS side is to call `console.log`
    
    :::javascript
    function log(args) {...}

The `register` function is called after each SPR item to write it to the HTML form. It is critical that this function is defined (and writes the results to form fields), or your results will not be saved.

    :::javascript
    function register(field, answer) {...}

The `onExperimentEnd` function is called after the last trial. This allows you to call some arbitrary JS to, e.g. display a debriefing screen, a post-test questionnaire, enable the submit button, etc.

    :::javascript
    function onExperimentEnd() {...}

# Flash parameters

Currently, there are two Flash parameter that the applet looks for, and one is mandatory. `exp` is the name of the directory under `flexspr/` where the config and item files for this experiment (or list) are located.
`path` is the base path below `flexspr/` on your web server, which defaults to `/`. 

# Embedding in a web page
It's best to use [SWFObject](https://cdnjs.com/libraries/swfobject) to embed a Flash object in a cross browser way. Be sure to have an empty HTML div called `flashcontent` for the example below.

```javascript
function equal_arrays(a1, a2) {
    'use strict';
    return (a1.length === a2.length) && a1.every(function(e, i) {
        return e === a2[i];
    });
}

var flashref = null;
function flashLoadedCB(e) {
    'use strict';
    if (e.success) {
        flashref = e.ref;
    } else {
        if (equal_arrays(swfobject.ua.pv, [0,0,0])) {
            document.getElementById('flashcontent').innerHTML = '<p style="font-weight bold; color: red;">Flash plugin is not installed or is disabled</p>';
        } else {
            document.getElementById('flashcontent').innerHTML = '<p style="font-weight bold; color: red;">Failed to load flash content.</p>';
        }
    }
}

var swfurl = '<URL to your your applet>';
var flashvars = {'exp': '<your experiment name>', 'path': '<base path to spr files>'};
var params = {allowScriptAccess: 'always', menu: 'false', bgcolor:'#ffffff', quality: 'high'};

swfobject.embedSWF(swfurl,
                  'flashcontent', '950', '600', '24.0.0', null,
                  flashvars, params, {}, function(e) {flashLoadedCB(e);});
```

n.b Flex 4.16 requires Flash version >= 24.0.0

n.b Browsers are starting to block Flash by default. As of early 2017:

  - Chrome: Disabled by default. In advanced preferences you can whitelist specific sites. Will be removed entirely within next year.
  - Firefox: Enabled by default, but will be disabled by default by Fall, removed in 2018.
  - Safari: Disabled by default. `preferences  -> Plug-in settings…` and then enable Flash and add websites by toggling it for specific sites currently open in a tab or window
  - Edge: Disabled by default, but can be toggled on

# Config file variables

These variables are set in the `config` file for each experiment directory. Syntax is `set\t<variable>\t<value>` (where \t indicates a tab character).

Variable       | Default value | Meaning
--------       | ------------- | -------
presentation   | SPR           | Do 'SPR' or 'SMS' presentation
insertBreaks   | false         | Automatically insert line breaks if sentence is wider than lineWidth + round(rnorm() * widthVariation)
lineWidth      | 60            | Only used if insertBreaks is true. Max width before break
widthVariation | 5             | Only used if insertBreaks is true.
randomize      | true          | Randomize item presentation. If this is true, but items aren't fully crossed, applet will crash.
feedback       | true          | Give feedback on whether answers to questions are correct